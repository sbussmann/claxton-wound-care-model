##############################################################################
# Project: Claxton Wound Care
# Date: 9/29/2014
# Author: Sam Bussmann
# Description: Do some mapping
# Notes: 
##############################################################################

# library(MASS)
library(RColorBrewer)
library(maptools)
# library(PBSmapping)
library(ggmap)
library(akima)
library(reshape2)
library(gridExtra)
# library(rgdal)
# library(rgeos)
# library(extrafont)
# library(mapproj)

# font_import(pattern="Symbola", prompt=FALSE)
# loadfonts()

#load("pll.RData")
load("combined_score_df.RData")
#load("hosploc.RData")

## 2010 census zips -- imported from shapefile
# rwa <- readOGR(dsn = "F:/zip_shapefile", layer = "tl_2013_us_zcta510")
# save(rwa,file="F:/zip_shapefile/zips.RData")
load("F:/zip_shapefile/zips.RData")


## Do deciling on individuals (previously done using addresses)
b<-10
qqp<-unique(quantile(tog$score,probs=seq.int(0,1, length.out=b+1)))
outp <- cut(tog$score, breaks=qqp, include.lowest=TRUE, labels=as.character(c(10:1)))

### Get lat/long for patients

mapme<-merge(tog,pll[,c("CommunityPersonID_orig","longitude","latitude")],
             by.x=c("CommunityPersonID"),by.y=c("CommunityPersonID_orig"),all.x=T)
temp<-mapme[!is.na(mapme$longitude),c("longitude","latitude")]
deciletemp<-outp[!is.na(mapme$longitude)]
scoretemp<-tog$score[!is.na(mapme$longitude)]
rawdata <- data.frame(temp$longitude, temp$latitude,fac2num(deciletemp),scoretemp)
names(rawdata) <- c("lon", "lat","decile","score")

TID<-78
pt_ind<-(!is.na(pm$PayerType[pm$TenantID==TID]))
pt_flag<-((pm$pt_flag[pm$TenantID==TID]==1))
rawdata_pt<-rawdata[pt_ind,]
rawdata_ptf<-rawdata[pt_flag,]

### Get boundaries of zip codes ready
sql<-odbcConnect("IrmSqls")
zips<-sqlQuery(sql,paste0("SELECT distinct Zipcode 
                    from ", tenant[tenant$TenantID==TID,"DatabaseName"], ".dbo.starkarea"),errors=T,stringsAsFactors = F,as.is=T)

index<-which(rwa[[1]] %in% zips[[1]])
rwa2 <- fortify(rwa[rwa[[1]] %in% zips[[1]],])

## Any zips in data not in boundaries?

zips[which(!(zips[[1]] %in% rwa[[1]])),]

### Get smoothed data

### Interpolate data to regular grid for heat maps

# fld <- with(rawdata, interp(x = lon, y = lat, z = score, duplicate="mean",xo=seq(min(lon), max(lon), length = 200),
#                             yo=seq(min(lat), max(lat), length = 200),linear=T))
# 
# df <- melt(fld$z, na.rm = TRUE)
# names(df) <- c("x", "y", "score")
# df$Lon <- fld$x[df$x]
# df$Lat <- fld$y[df$y]


fldd <- with(rawdata, interp(x = lon, y = lat, z = decile, duplicate="mean",xo=seq(min(lon), max(lon), length = 40),
                             yo=seq(min(lat), max(lat), length = 40),linear=T))

dfd <- melt(fldd$z, na.rm = TRUE)
names(dfd) <- c("x", "y", "decile")
dfd$Lon <- fldd$x[dfd$x]
dfd$Lat <- fldd$y[dfd$y]

### Create bounding box and get google map

bb<-make_bbox(lon,lat,rawdata,f=.1)
hdf <- get_map(location=bb,maptype="roadmap",color = "bw")

### Dot Density of all patients
# 
# plot1.1<-ggmap(hdf, extent = "panel") +
#   geom_point(aes(x = lon, y = lat), shape=".", data = rawdata_pt, alpha = .09, colour="red") +
#   scale_fill_hue(l=95) + 
#   geom_text(data=hosploc, aes(x=Longitude,y=Latitude, label="+", fontface="bold"),size = 5,colour="red3") +
#   theme(legend.position = "none", axis.title = element_blank(), text = element_text(size = 12),
#         axis.line=element_blank(), 
#         axis.text.x=element_blank(), 
#         axis.text.y=element_blank(), 
#         axis.ticks=element_blank(), 
#         axis.ticks.length=unit(0.3, "lines"), 
#         axis.ticks.margin=unit(0.5, "lines"), 
#         axis.title.x=element_blank(), 
#         axis.title.y=element_blank(),
#         plot.margin=unit(c(2, 2, 1, 1), "lines")) +
#         ggtitle("Dot Density of All Patients")
# 
# 
# pdf("map4.pdf")
# plot1.1
# dev.off()
# 
# ### Dot Density of pts with target
# 
# plot1.2<-ggmap(hdf, extent = "panel") +
#   geom_point(aes(x = lon, y = lat), shape=".", data = rawdata_ptf, alpha = .8, colour="red") +
#   scale_fill_hue(l=95) + 
#   geom_text(data=hosploc, aes(x=Longitude,y=Latitude, label="+", fontface="bold"),size = 5,colour="red3") +
#   theme(legend.position = "none", axis.title = element_blank(), text = element_text(size = 12),
#         axis.line=element_blank(), 
#         axis.text.x=element_blank(), 
#         axis.text.y=element_blank(), 
#         axis.ticks=element_blank(), 
#         axis.ticks.length=unit(0.3, "lines"), 
#         axis.ticks.margin=unit(0.5, "lines"), 
#         axis.title.x=element_blank(), 
#         axis.title.y=element_blank(),
#         plot.margin=unit(c(2, 2, 1, 1), "lines")) +
#   ggtitle("Dot Density of Lung Cancer Patients")
# 
# 
# pdf("map4.1.2.pdf")
# plot1.2
# dev.off()

### Density map of patients in area

hdf2 <- get_map(location=c(hosploc$Longitude[1],hosploc$Latitude[1]),zoom=10,maptype="roadmap",color = "bw")

# dens <- kde2d(rawdata_ptf$lon, rawdata_ptf$lat, n = 1000,h=c(.0426/2,.0288/2))
# densdf <- data.frame(expand.grid(lon = dens$x, lat = dens$y),
#                      z = as.vector(dens$z))


plot2.1<-ggmap(hdf, extent = "device", maprange=FALSE) +
  geom_density2d(aes(x = lon, y = lat), data = rawdata_pt) +
  stat_density2d(data = rawdata_pt, aes(x = lon, y = lat,  fill = ..level.., alpha = ..level.., h=10),
                 size = 0.01, bins = 20, geom = 'polygon') +
  scale_fill_gradient(low = "white", high = "blue") +
  scale_alpha(range = c(0.05, 0.15), guide = FALSE) +
  geom_text(data=hosploc, aes(x=Longitude,y=Latitude, label="+", fontface="bold"),size = 8,colour="red3") +
  theme(legend.position = "none", axis.title = element_blank(), text = element_text(size = 12),
        axis.line=element_blank(), 
        axis.text.x=element_blank(), 
        axis.text.y=element_blank(), 
        axis.ticks=element_blank(), 
        axis.ticks.length=unit(0.3, "lines"), 
        axis.ticks.margin=unit(0.5, "lines"), 
        axis.title.x=element_blank(), 
        axis.title.y=element_blank(),
        plot.margin=unit(c(.25, .25, .25, .25), "lines")) +
  ggtitle("Density Map of All Patients")

pdf("Map_Density_All_Patients.pdf")
plot2.1
dev.off()

### Density map of target patients

plot2<-ggmap(hdf, extent = "device", maprange=FALSE) +
  geom_density2d(aes(x = lon, y = lat), data = rawdata_ptf) +
  stat_density2d(data = rawdata_ptf, aes(x = lon, y = lat,  fill = ..level.., alpha = ..level.., h=10),
                 size = 0.01, bins = 20, geom = 'polygon') +
  scale_fill_gradient(low = "white", high = "blue") +
  scale_alpha(range = c(0.05, 0.15), guide = FALSE) +
  geom_text(data=hosploc, aes(x=Longitude,y=Latitude, label="+", fontface="bold"),size = 8,colour="red3") +
  theme(legend.position = "none", axis.title = element_blank(), text = element_text(size = 12),
        axis.line=element_blank(), 
        axis.text.x=element_blank(), 
        axis.text.y=element_blank(), 
        axis.ticks=element_blank(), 
        axis.ticks.length=unit(0.3, "lines"), 
        axis.ticks.margin=unit(0.5, "lines"), 
        axis.title.x=element_blank(), 
        axis.title.y=element_blank(),
        plot.margin=unit(c(.25, .25, .25, .25), "lines")) +
  ggtitle("Density Map of Target Patients")

pdf("Map_Density_Target_Group.pdf")
plot2
dev.off()

### heat map of decile

g2 <- ggmap(hdf, extent = "device", maprange=FALSE) +
  geom_tile(data = dfd, aes(x = Lon, y = Lat, z = decile, fill = decile), alpha = 0.4) +
  scale_fill_continuous(name = "Decile",
                        low = "blue", high = "white") +
  geom_text(data=hosploc, aes(x=Longitude,y=Latitude, label="+", fontface="bold"),size = 8,colour="red3") +
  
theme(axis.title = element_blank(), text = element_text(size = 12),
      axis.line=element_blank(), 
      axis.text.x=element_blank(), 
      axis.text.y=element_blank(), 
      axis.ticks=element_blank(), 
      axis.ticks.length=unit(0.3, "lines"), 
      axis.ticks.margin=unit(0.5, "lines"), 
      axis.title.x=element_blank(), 
      axis.title.y=element_blank(),
      plot.margin=unit(c(.25, .25, .25, .25), "lines")) +
  ggtitle("Smoothed (Averaged) Decile")
  
pdf("Map_Smoothed_Decile.pdf")
g2
dev.off()

### A thematic map for decile


## Get data to plot in each zip code

# mapme<-merge(tog,pll[,c("CommunityPersonID","longitude","latitude")],by=c("CommunityPersonID"),
#              all.x=T)
# mapme<-merge(tog,pll[,c(1,3,4)],by=c("CommunityPersonID"),all.x=T)
t2t<-mapme[!is.na(mapme$longitude),"Zipcode"]
deciletemp<-outp[!is.na(mapme$longitude)]

meanbyzip<-by(fac2num(deciletemp),t2t,mean,na.rm=T)
mbz<-round(as.numeric(meanbyzip),4)
#ztemp<-data.frame(zipcode=row.names(meanbyzip),use=round(as.numeric(meanbyzip),3))

ztemp<-data.frame(zipcode=row.names(meanbyzip),use=mbz)

## Add zip code level data to shapefile data
lookup<-data.frame(rwa[rwa[[1]] %in% zips[[1]],1],index=row.names(rwa[rwa[[1]] %in% zips[[1]],]))
dimnames(lookup)[[2]][1]<-"zipcode"
rwa3<-merge(rwa2,lookup,by.x=c("id"),by.y=c("index"),all.x=T)
rwa4<-merge(ztemp,rwa3,by=c("zipcode"),all.y=T)
rwa5<-rwa4[order(rwa4$id,rwa4$order),]


# bb1<-make_bbox(lon,lat,rawdata,f=.24)
# hdf1 <- get_map(location=bb1,maptype="roadmap",color = "bw")
#hdf1 <- get_map(location=c(hosploc$Longitude,hosploc$Latitude),zoom=10,maptype="roadmap",color = "bw")

g4 <- ggmap(hdf, extent = "normal", maprange=FALSE) +
  geom_polygon(data = rwa5, aes(x = long, y = lat, group = group, fill = use),
               colour = "black", size = 0.5, alpha=.4) +
  scale_fill_continuous(name = "Decile",
                        low = "blue", high = "white") +
  geom_text(data=hosploc, aes(x=Longitude,y=Latitude, label="+", fontface="bold"),size = 8,colour="red3") +
  coord_map(projection="mercator", 
            xlim=c(attr(hdf, "bb")$ll.lon, attr(hdf, "bb")$ur.lon),
            ylim=c(attr(hdf, "bb")$ll.lat, attr(hdf, "bb")$ur.lat)) +
  #geom_path(data=circdf, aes(x=Longitude,y=Latitude),lineend = "round", colour="red3")+
  theme(axis.title = element_blank(), text = element_text(size = 12),
        axis.line=element_blank(), 
        axis.text.x=element_blank(), 
        axis.text.y=element_blank(), 
        axis.ticks=element_blank(), 
        axis.ticks.length=unit(0.3, "lines"), 
        axis.ticks.margin=unit(0.5, "lines"), 
        axis.title.x=element_blank(), 
        axis.title.y=element_blank(),
        plot.margin=unit(c(.25,.25, .25, .25), "lines")) +
  ggtitle("Average Decile by ZipCode")

g4

pdf("Map_Decile_by_ZipCode.pdf")
g4
dev.off()




############ Below not run ###############################

### Plot a circle with radius= median distance traveled

## 1 mile is equal to .01447 latitude

## Use a equidistant projection to find the circle in that coordinate system
## Then project back to lat/long and plot

med<-2.806
forradius<-.01477*med

latlonrad<-c(as.numeric(hosploc[1,2]),as.numeric(hosploc[1,3])+forradius)
xy<-matrix(c(as.numeric(hosploc[,c(2,3)]),latlonrad),2,2,byrow=T)

test<-project(xy,proj="+proj=eqdc +lon_0=90w +lat_1=20n +lat_2=60n")
dia<-dist(test)*2

circleFun <- function(center = c(0,0),diameter = 1, npoints = 100){
  r = diameter / 2
  tt <- seq(0,2*pi,length.out = npoints)
  xx <- center[1] + r * cos(tt)
  yy <- center[2] + r * sin(tt)
  return(data.frame(x = xx, y = yy))
}

circpts<-circleFun(test[1,],dia,1500)


circpts_proj<-project(as.matrix(circpts),proj="+proj=eqdc +lon_0=90w +lat_1=20n +lat_2=60n",inv=T)
circdf<-data.frame(Longitude=circpts_proj[,1],Latitude=circpts_proj[,2])


bb1<-make_bbox(lon,lat,rawdata,f=.24)
hdf_radius <- get_map(location=bb1,maptype="roadmap")#,color = "bw")

#hdf_radius <- get_map(location=c(hosploc$Longitude,hosploc$Latitude),zoom=12,maptype="roadmap")#,color = "bw")

g5 <- ggmap(hdf_radius, extent = "normal", maprange=FALSE) +
  geom_path(data = rwa5, aes(x = long, y = lat, group = group),
               colour = "darkorchid3", size = 0.5, alpha=1) +
  geom_text(data=hosploc, aes(x=Longitude,y=Latitude, label="+", fontface="bold"),size = 6,colour="red3") +
  coord_map(projection="mercator", 
            xlim=c(attr(hdf1, "bb")$ll.lon, attr(hdf1, "bb")$ur.lon),
            ylim=c(attr(hdf1, "bb")$ll.lat, attr(hdf1, "bb")$ur.lat)) +
  geom_path(data=circdf, aes(x=Longitude,y=Latitude,colour="red3"),lineend = "round", size=.5)+
  scale_colour_manual(name="Median Distance Traveled",labels="Radius = 2.8 miles",values="red3") + 
  theme(axis.title = element_blank(), text = element_text(size = 12),
        axis.line=element_blank(), 
        axis.text.x=element_blank(), 
        axis.text.y=element_blank(), 
        axis.ticks=element_blank(), 
        axis.ticks.length=unit(0.3, "lines"), 
        axis.ticks.margin=unit(0.5, "lines"), 
        axis.title.x=element_blank(), 
        axis.title.y=element_blank(),
        plot.margin=unit(c(.25,1, .25, .25), "lines")) +
  ggtitle("Median Distance Traveled for All Patients")

g5

pdf("map6.15.pdf")
g5
dev.off()


### Close up of radius

hdf_radius <- get_map(location=c(hosploc$Longitude,hosploc$Latitude),zoom=12,maptype="roadmap")#,color = "bw")

g6 <- ggmap(hdf_radius, extent = "device", maprange=FALSE) +
  geom_path(data = rwa5, aes(x = long, y = lat, group = group),
            colour = "darkorchid3", size = 0.5, alpha=1) +
  geom_text(data=hosploc, aes(x=Longitude,y=Latitude, label="+", fontface="bold"),size = 8,colour="red3") +
  geom_path(data=circdf, aes(x=Longitude,y=Latitude,colour="red3"),lineend = "round", size=.6)+
  scale_colour_manual(name="Median Distance Traveled",labels="Radius = 2.8 miles",values="red3") + 
  theme(axis.title = element_blank(), text = element_text(size = 12),
        axis.line=element_blank(), 
        axis.text.x=element_blank(), 
        axis.text.y=element_blank(), 
        axis.ticks=element_blank(), 
        axis.ticks.length=unit(0.3, "lines"), 
        axis.ticks.margin=unit(0.5, "lines"), 
        axis.title.x=element_blank(), 
        axis.title.y=element_blank(),
        plot.margin=unit(c(.25,1, .25, .25), "lines")) +
  ggtitle("Median Distance Traveled for All Patients")

g6

pdf("map7.1.pdf")
g6
dev.off()
